package ru.sber.jd.collections;


public class MyStack<T> {

    static class Element<T> {
        T value;
        Element<T> next;

        Element(T value, Element<T> next) {
            this.value = value;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {

            if (this.value.equals(((Element<T>) o).value) && this.next == (((Element<T>) o).next)) {
                return true;
            } else {
                return false;
            }


        }

    }

    static class MinElement<T> {
        Element<T> element;
        MinElement<T> next;

        MinElement(Element<T> element, MinElement<T> next) {
            this.element = element;
            this.next = next;
        }

    }


    private int size = 0;
    private MinElement<T> min;
    private Element<T> current;


    public MyStack() {
    }

    public String printStackMin() {
        MinElement<T> me = this.min;
        String result = "";

        if (this.current != null) {
            result = result + me.element.value;

            while ((me = me.next) != null) {
                result = result + " " + me.element.value;
            }
        }
        return (result == "" ? null : result);
    }

    public int getSize() {
        return this.size;
    }

    public T getCurrent() {
        if (this.current == null) {
            return null;
        } else {
            return this.current.value;
        }
    }

    public T getMin() {
        if (this.min != null) {
            return this.min.element.value;
        } else {
            return null;
        }
    }

    public void remove() {

        if (this.current.next == null) {
            this.size--;
            this.current = null;
            this.min = null;
        } else {

            //если удаляемый объект минимальный
            if (this.current.equals(this.min.element)) {
                MinElement<T> minTemp = this.min.next;
                this.min = new MinElement(minTemp.element, minTemp.next);
            }

            this.size--;
            Element<T> temp = this.current.next;
            this.current = new Element(temp.value, temp.next);

        }
    }


    public void add(T value) {
        //если это первый элемент, то записываем в текущий
        if (this.current == null) {
            this.size++;
            this.current = new Element(value, null);
            this.min = new MinElement(this.current, null);

            //если не первый
        } else {
            this.size++;
            Element<T> temp = this.current;
            this.current = new Element(value, temp);

            //сравниваем с минимальным

            if (value.toString().compareTo(this.min.element.value.toString()) > 0) {
                MinElement<T> minTemp = min;
                this.min = new MinElement(this.current, minTemp);
            }

        }


    }


    @Override
    public String toString() {
        String result = "";
        Element<T> e;

        if (this.current == null) {
            return null;
        } else {
            result = "" + this.current.value;
            e = this.current;
            while ((e = e.next) != null) {
                result = result + " " + e.value;
            }


        }


        return result;
    }


}


