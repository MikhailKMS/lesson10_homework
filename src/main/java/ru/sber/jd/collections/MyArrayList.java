package ru.sber.jd.collections;

import java.util.Arrays;

public class MyArrayList<T> {

    private int size = 0;
    private int capacity = 0;
    private Object[] elementData = new Object[capacity];


    public void add(T newElement) {
        if (ensureCapacity(this.size + 1)) {
            this.elementData[this.size] = newElement;
            this.size++;

        }
    }

    public void remove() {
        this.size--;
        this.elementData[this.size] = null;
    }

    public int size() {
        return this.size;
    }


    private boolean ensureCapacity(int size) {
        if (size <= this.capacity) {
            return true;
        } else {

            Object[] elementDataForCopy;
            elementDataForCopy = Arrays.copyOf(this.elementData, this.capacity);

            this.capacity = (this.capacity * 3) / 2 + 1;
            this.elementData = new String[this.capacity];
            this.elementData = Arrays.copyOf(elementDataForCopy, this.capacity);

            return true;
        }
    }


    public MyArrayList() {
    }

    public MyArrayList(int capacity) {
        this.capacity = capacity;
        this.elementData = new Object[capacity];
    }

    @Override
    public String toString() {
        String result = "";

        for (int i = 0; i < this.capacity; i++) {
            if (i > 0) {
                result = result + " " + this.elementData[i];
            } else {
                result = result + this.elementData[i];
            }
        }
        return result;
    }


}
