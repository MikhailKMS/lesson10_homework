package ru.sber.jd.collections;


import java.util.Arrays;
import java.util.Objects;


public class MyHashMap<K, V> {



    static class Node<K, V> {
        int hash;
        K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        public final String toString() {
            return this.key + ":" + this.value;
        }

        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        public V setValue(V newValue) {
            V oldValue = this.value;
            this.value = newValue;
            return oldValue;
        }

        public boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Node) {
                Node<K, V> e = (Node<K, V>) o;
                if (Objects.equals(this.key, e.getKey()) &&
                        Objects.equals(this.value, e.getValue()))
                    return true;
            }
            return false;
        }

    }


    private Node<K, V>[] table;
    private int size;
    private float loadFactor;
    private int capacity;
    private int threshold;

    public MyHashMap() {
        this.size = 0;
        this.loadFactor = 0.75f;
        this.capacity = 2;
        this.threshold = (int) (this.capacity * this.loadFactor);
        //this.table = (Node<K, V>[]) new Object[this.capacity];
        this.table = new Node[this.capacity];
    }

    public Node<K, V>[] resize() {

        Node<K, V>[] tableForCopy = Arrays.copyOf(this.table, this.capacity);
        this.size = 0;
        this.capacity = this.capacity * 2;
        //this.table = (Node<K, V>[]) new Object[this.capacity];
        this.table = new Node[this.capacity];
        this.threshold = (int) (this.capacity * this.loadFactor);


        for (int i = 0; i < tableForCopy.length; i++) {

            if (tableForCopy[i] != null) {

                put(tableForCopy[i].key, tableForCopy[i].value);

                Node<K, V> e;
                Node<K, V> p = tableForCopy[i];
                for (int binCount = 0; ; ++binCount) {

                    if ((p != null) && (e = p.next) != null) {
                        put(e.key, e.value);
                        p = e.next;
                    } else {
                        break;
                    }
                }
            }
        }
        return this.table;
    }

    public Integer size() {
        return this.size;
    }


    static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }


    public V put(K key, V value) {

        Node<K, V>[] tab;
        Node<K, V> p;
        int n;
        int i;
        int hash = hash(key);

        //если таблица не создана
        if ((tab = this.table) == null || (n = tab.length) == 0) {
            n = (tab = resize()).length;
        }
        //если бакет пустой
        if ((p = tab[i = (n - 1) & hash]) == null) {
            tab[i] = new Node<K, V>(hash, key, value, null);
            this.size++;
            //если бакет не пустой
        } else {
            Node<K, V> e;
            K k;
            //сравниваем ключи у первого элемента в списке в бакете
            if (p.hash == hash && ((k = p.key) == key || (key != null && key.equals(k)))) {
                e = p;
            } else {
                //проходим по элементам в списке в бакете
                for (int binCount = 0; ; ++binCount) {

                    if ((e = p.next) == null) {
                        p.next = new Node<K, V>(hash, key, value, null);
                        this.size++;
                        break;
                    }
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k))))
                        break;

                    p = e;
                }
            }
            //перезаписать значение у ключа, если пришел одинаковый ключ
            if (e != null) {
                V oldValue = e.value;
                e.value = value;

                return oldValue;
            }

        }

        if (this.size > this.threshold) {
            resize();
        }

        return value;
    }


    public V get(Integer key) {

        Node<K, V>[] tab;
        Node<K, V> first, e;
        int n;
        K k;
        int hash = hash(key);

        //если таблица не пустая и в бакете по хешу ключа есть элементы
        if ((tab = this.table) != null && (n = tab.length) > 0 &&
                (first = tab[(n - 1) & hash]) != null) {
            //если ключ первого элемента совпадает с искомым
            if (first.hash == hash && ((k = first.key) == key || (key != null && key.equals(k)))) {
                return first.value;
            }
            //перебираем связанный список в поисках ключа
            if ((e = first.next) != null) {
                do {
                    if (e.hash == hash && ((k = e.key) == key || (key != null && key.equals(k))))
                        return e.value;
                } while ((e = e.next) != null);
            }

        }
        return null;
    }

    public K containValue(V value) {

        Node<K, V> first, e;
        K k;

        //пробегаемся по всей хеш-таблице
        for (int i = 0; i < this.table.length; i++) {

            //если первый элемент не пустой
            if ((first = this.table[i]) != null) {
                //если значение первого элемента совпадает с искомым
                if (first.value.equals(value)) {
                    return first.key;
                }
                //перебираем связанный список в поисках значения
                if ((e = first.next) != null) {
                    do {
                        if (e.value.equals(value))
                            return e.key;
                    } while ((e = e.next) != null);
                }
            }
        }
        return null;
    }


    @Override
    public String toString() {
        String result = "";

        for (int i = 0; i < this.capacity; i++) {

            result = result + " " + i + ") " + this.table[i];

            if (this.table[i] != null) {
                Node<K, V> e = this.table[i];
                for (int binCount = 0; ; ++binCount) {
                    if (e.next != null) {
                        result = result + " -> " + e.next;
                        e = e.next;
                    } else {
                        result = result + "\n";
                        break;
                    }
                }
            } else {
                result = result + "\n";
            }

        }
        return result;
    }

}
