package ru.sber.jd;

import ru.sber.jd.collections.MyArrayList;
import ru.sber.jd.collections.MyHashMap;
import ru.sber.jd.collections.MyStack;

public class Main {

    public static void main(String[] args) {

        MyArrayList<String> myArrayList1 = new MyArrayList<String>();
        MyArrayList<Integer> myArrayList2 = new MyArrayList<Integer>();

        myArrayList1.add("Hello");
        myArrayList1.add("World");
        myArrayList1.add("Forever");
        myArrayList1.add("!!!");
        myArrayList1.remove();
        myArrayList1.remove();
        System.out.println("Реализация myArrayList\nЗначение myArrayList для типа String: " + myArrayList1);
        System.out.println("Количество элементов myArrayList: " + myArrayList1.size());

        myArrayList2.add(0);
        myArrayList2.add(1);
        myArrayList2.add(2);
        myArrayList2.add(3);
        myArrayList2.remove();
        myArrayList2.remove();
        System.out.println("\nЗначение myArrayList для типа Integer: " + myArrayList2);
        System.out.println("Количество элементов myArrayList: " + myArrayList2.size());


        MyHashMap<Integer, String> myHashMap = new MyHashMap<Integer, String>();

        myHashMap.put(1, "FirstValue");
        myHashMap.put(2, "SecondValue");
        myHashMap.put(20, "20Value");
        myHashMap.put(24, "24Value");
        myHashMap.put(21, "21Value");
        myHashMap.put(22, "22Value");
        myHashMap.put(23, "23Value");
        myHashMap.put(1, "New_FirstValue");
        myHashMap.put(1, "New2_FirstValue");
        myHashMap.put(20, "New_20Value");
        myHashMap.put(24, "New_24Value");

        System.out.println("\nРеализация HashMap\n"+myHashMap);

        System.out.println("Количество элементов: " + myHashMap.size());
        System.out.println("Значение элемента HashMap по ключу 24 = " + myHashMap.get(24));
        System.out.println("Ключ элемента HashMap для значения 'New_24Value' = " + myHashMap.containValue("New_24Value"));




        MyStack<Integer> myStack = new MyStack<Integer>();

        myStack.add(-1);
        myStack.add(-5);
        myStack.add(-3);
        myStack.add(-6);
        myStack.add(-7);

        System.out.println("\nРеализация Стэка\nСтэк: " + myStack);
        System.out.println("Стэк минимальных элементов: " + myStack.printStackMin());
        System.out.println("Количество значений в Стэке = " + myStack.getSize());
        System.out.println("Текущий элемент = " + myStack.getCurrent());
        System.out.println("Значение минимального элемента в Стэке = " + myStack.getMin());

        myStack.remove();
        myStack.remove();
        myStack.remove();
        myStack.remove();
        myStack.add(-7);
        myStack.remove();

        System.out.println("\nСтэк: " + myStack);
        System.out.println("Стэк минимальных элементов: " + myStack.printStackMin());
        System.out.println("Количество значений в Стэке = " + myStack.getSize());
        System.out.println("Текущий элемент = " + myStack.getCurrent());
        System.out.println("Значение минимального элемента в Стэке = " + myStack.getMin());


    }


}
